﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//starterfile
public class PlayerController : MonoBehaviour
{

    public float speed;
    public float jumpPower;
    bool onGround;
    public Text countText;
    public Text winText;
    public Text coinText;
    public GameObject pickupPrefab;
    public GameObject coinPickupPrefab;
    float timePassed;
    int totalPickups;
    //public GameObject westWall;
    //public Material westGreen;

    Rigidbody rb;
    Renderer rend;

    private int count;
    private int coinCount;
    public GameObject doorway;

    // Start is called before the first frame update
    // establish variables
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
        count = 0;
        coinCount = 0;
        winText.text = "";

        GameObject[] allPickups = GameObject.FindGameObjectsWithTag("Pick Up");
        GameObject[] allCoins = GameObject.FindGameObjectsWithTag("Coin");
        totalPickups = allPickups.Length;
        print("total Pickups: " + totalPickups);
        SetCountText();
    }

    private void Update()
    {
        //sets onGround so you cannot jump multiple times
        onGround = Physics.Raycast(transform.position, Vector3.down, .55f);
        //instantiates jump
        if (Input.GetKeyDown(KeyCode.Space)&& onGround)
        {
            Jump();
        }
        timePassed += Time.deltaTime;
        //print(timePassed);
        /*if (timePassed > 20f)
        {
            //print("GAME OVER!");
        }*/
        if (count >= 20)
        {
            Destroy(doorway);
        }
        //new variable win condition for picked up coins
        if (coinCount >= 13)
        {
            winText.text = "#1 Victory Royale!!!";
        }
    }
    //realistic jump added
    void Jump()
    {
        //Destroy(doorway);
        //add upward force to make character jump
        rb.AddForce(Vector3.up * jumpPower);

    }
    

    // Update is called once per frame
    void FixedUpdate()
    {
        //movement code
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        //checks tag of game object for pick up initial prefab
        if (other.gameObject.CompareTag("Pick Up"))
        {
            //code allowing pickup to be picked up
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        //checks tag of game object for coin initial prefab
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            coinCount = coinCount + 1;
            SetCoinText();
        }
    }
    //collision change color of ball to material of any "Wall" tag
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            rend.material = collision.gameObject.GetComponent<Renderer>().material;
        }

    }

    void SetCountText()
    {
        //sets count to total picked up pickups and spawns random one if greater than amount currently available
        countText.text = "Count: " + count.ToString();
        if (count >= totalPickups)
        {
            SpawnRandomPickup();
            //winText.text = "You Win!";
        }
    }
    //new coin variable for text of new CoinText variable set here
    void SetCoinText()
    {
        coinText.text = "Coin Count: " + coinCount.ToString();
    }
    void SpawnRandomPickup()
    {//pick random position to place prefab with random x and z and constant y
        Vector3 randomPosition = new Vector3(Random.Range(-5f,5f),.5f,Random.Range(-5f,5f));
        //make a copy of pickupPrefab at randomPosition with original prefab rotation
        Instantiate(pickupPrefab, randomPosition, pickupPrefab.transform.rotation);

        totalPickups++;
    }
}



//transform.localScale += new Vector3(.1f,.1f,.1f);
//speed += 1f;